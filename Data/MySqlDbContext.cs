using Backend.Models;
using Microsoft.EntityFrameworkCore;

namespace Backend.Data
{
    public class MySqlDbContext : DbContext
    {
        public MySqlDbContext (DbContextOptions<MySqlDbContext> options)
            : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
    }
}