using System.Linq;
using Backend.Models;

namespace Backend.Data
{
    public class DbInitializer
    {
        public static void Initialize(MySqlDbContext context)
        {
            context.Database.EnsureCreated();

            if (context.Users.Any())
            {
                return;
            }

            var users = new User[]
            {
                new User { FirstName = "Sanek", Email = "sanek@gmail.com" },
                new User { FirstName = "Timur", Email = "timur@gmail.com" },
                new User { FirstName = "Max", Email = "max@gmail.com" },
                new User { FirstName = "Artem", Email = "artem@gmail.com" },
                new User { FirstName = "Vadim", Email = "vadim@gmail.com" },
            };

            context.Users.AddRange(users);

            context.SaveChanges();
        }
    }
}